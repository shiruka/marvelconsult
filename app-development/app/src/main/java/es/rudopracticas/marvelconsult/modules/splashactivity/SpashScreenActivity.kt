package es.rudopracticas.marvelconsult.modules.splashactivity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.modules.login.LoginActivity

class SpashScreenActivity : AppCompatActivity() {

    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spash_screen)
        viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
        openActivity()
    }

    private fun openActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }


}
