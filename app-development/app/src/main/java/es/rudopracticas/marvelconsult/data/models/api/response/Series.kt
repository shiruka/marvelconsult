package es.rudopracticas.marvelconsult.data.models.api.response

data class Series(
    val name: String,
    val resourceURI: String
)