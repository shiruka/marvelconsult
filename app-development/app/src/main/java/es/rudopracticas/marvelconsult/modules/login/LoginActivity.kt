package es.rudopracticas.marvelconsult.modules.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.databinding.ActivityLoginBinding
import es.rudopracticas.marvelconsult.helpers.extensions.isValidEmail
import es.rudopracticas.marvelconsult.modules.home.HomeActivity
import es.rudopracticas.marvelconsult.modules.register.RegisterActivity


class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        auth = FirebaseAuth.getInstance()
        initListeners()
    }

    private fun initListeners() {
        binding.buttonLogin.setOnClickListener {
            doLogin()
        }
        binding.singUpButton.setOnClickListener {
            intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    private fun doLogin() {
        binding.buttonLogin.isEnabled = false

        if (binding.edittextUsernameLogin.text.toString().isEmpty()) {
            binding.edittextUsernameLogin.error = getString(R.string.text_errorInput_enterEmail)
            binding.edittextUsernameLogin.requestFocus()
            return
        }
        if (!binding.edittextUsernameLogin.text.toString().isValidEmail()
        ) {

            binding.edittextUsernameLogin.error =
                getString(R.string.text_errorInput_enterEmailValid)
            binding.edittextUsernameLogin.requestFocus()
            return
        }

        if (binding.edittextPasswordLogin.text.toString().isEmpty()) {
            binding.edittextPasswordLogin.error = getString(R.string.text_errorInput_enterPassword)
            binding.edittextPasswordLogin.requestFocus()
            return
        }
        auth.signInWithEmailAndPassword(
            binding.edittextUsernameLogin.text.toString(),
            binding.edittextPasswordLogin.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("login", "signInWithEmail:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    Toast.makeText(
                        this,
                        getString(R.string.text_errorShow_failedLogin),
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(null)
                }
            }
    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(currentUser: FirebaseUser?) {
        if (currentUser != null) {
            Toast.makeText(
                application,
                getString(R.string.text_loginShow_greatings) + " " + currentUser.email,
                Toast.LENGTH_SHORT
            ).show()
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        } else {
            binding.buttonLogin.isEnabled = true
        }
    }
}


