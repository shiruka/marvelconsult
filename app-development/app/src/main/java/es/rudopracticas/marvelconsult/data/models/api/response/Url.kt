package es.rudopracticas.marvelconsult.data.models.api.response

data class Url(
    val type: String,
    val url: String
)