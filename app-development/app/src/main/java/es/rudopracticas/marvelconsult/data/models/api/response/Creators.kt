package es.rudopracticas.marvelconsult.data.models.api.response

data class Creators(
    val available: Int,
    val collectionURI: String,
    val items: List<Item>,
    val returned: Int
)