package es.rudopracticas.marvelconsult.data.models.api.response

data class Item(
    val name: String,
    val resourceURI: String,
    val role: String
)