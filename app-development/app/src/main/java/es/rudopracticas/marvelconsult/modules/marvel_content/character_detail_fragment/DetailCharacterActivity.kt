package es.rudopracticas.marvelconsult.modules.marvel_content.character_detail_fragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.data.models.api.response.MarvelResponse
import es.rudopracticas.marvelconsult.databinding.ActivityDetailCharacterBinding

class DetailCharacterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailCharacterBinding
    private lateinit var viewModel: DetailCharacterViewModel
    private lateinit var characterData: MarvelResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_character)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_character)
        viewModel = ViewModelProvider(this).get(DetailCharacterViewModel::class.java)
        binding.lifecycleOwner = this

        val itemUri = intent.getStringExtra("ITEM_ID")

        viewModel.getCharacterById(itemUri)

        viewModel.characterData.observe(this, Observer {
            characterData = it
            characterData.toString()
            showData()
        })

    }

    private fun showData() {

        val item = characterData.data.results[0]

        binding.characterNameActivity.text = item.name
        binding.textviewBioCharacterActivity.text = item.description as CharSequence?


        val imagePath = item.thumbnail.path + "/portrait_xlarge." + item.thumbnail.extension
        Picasso.get().load(imagePath).fit().centerCrop().into(binding.characterImageActivity)

    }
}
