package es.rudopracticas.marvelconsult.data.models.api.response

data class Price(
    val price: Double,
    val type: String
)