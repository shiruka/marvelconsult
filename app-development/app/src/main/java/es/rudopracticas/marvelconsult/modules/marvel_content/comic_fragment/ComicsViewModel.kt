package es.rudopracticas.marvelconsult.modules.marvel_content.comic_fragment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudopracticas.marvelconsult.api.Config
import es.rudopracticas.marvelconsult.api.RetrofitClient
import es.rudopracticas.marvelconsult.data.models.api.response.MarvelResponse
import es.rudopracticas.marvelconsult.helpers.Constants
import es.rudopracticas.marvelconsult.helpers.Constants.API_DATATYPE_COMIC
import es.rudopracticas.marvelconsult.helpers.extensions.md5
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class ComicsViewModel : ViewModel() {

    var comicData = MutableLiveData<MarvelResponse>()
    val query = MutableLiveData<String>()

    var isQuery = false
    var offset = 0
    private val ts = 1

    private var _eventIsRefreshing = MutableLiveData<Boolean>()
    val eventRefreshing: LiveData<Boolean>
        get() = _eventIsRefreshing

    private var hash = ts.toString() + Config.PRIVATE_KEY + Config.PUBLIC_KEY

    init {
        getComics()
    }

    fun getComics() {
        _eventIsRefreshing.value = true
        CoroutineScope(Dispatchers.IO).launch {

            RetrofitClient().apiCall({
                RetrofitClient().getComics(offset, ts, Config.PUBLIC_KEY, hash.md5())
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE && offset == 0) {
                            comicData.value?.data?.dataType = API_DATATYPE_COMIC
                            comicData.value = response.body() as MarvelResponse?
                        } else if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val mr = response.body() as MarvelResponse
                            val aux = comicData.value
                            aux?.data?.results?.addAll(mr.data.results)
                            aux?.data?.dataType = API_DATATYPE_COMIC
                            comicData.postValue(aux)
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }

                }
            )
            withContext(Dispatchers.Main) {
                _eventIsRefreshing.value = false
            }
        }

    }

    fun getComicsQuery(lastQuery: String) {
        _eventIsRefreshing.value = true
        CoroutineScope(Dispatchers.IO).launch {

            RetrofitClient().apiCall({

                RetrofitClient().getComicsQueryTitle(
                    offset,
                    ts,
                    Config.PUBLIC_KEY,
                    hash.md5(),
                    lastQuery
                )
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE && offset == 0) {
                            comicData.value = response.body() as MarvelResponse?
                            comicData.value?.data?.dataType = API_DATATYPE_COMIC
                        } else if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val mr = response.body() as MarvelResponse
                            val aux = comicData.value
                            aux?.data?.results?.addAll(mr.data.results)
                            aux?.data?.dataType = API_DATATYPE_COMIC
                            comicData.postValue(aux)
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }

                }
            )

            withContext(Dispatchers.Main) {
                _eventIsRefreshing.value = false
            }
        }

    }


}