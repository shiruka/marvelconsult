package es.rudopracticas.marvelconsult.modules.marvel_content.series_detail_fragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.adapters.DataAdapter
import es.rudopracticas.marvelconsult.data.models.api.response.MarvelResponse
import es.rudopracticas.marvelconsult.databinding.ActivityDetailSerieBinding
import java.lang.Exception

class DetailSerieActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailSerieBinding
    private lateinit var viewModel: DetailSeriesViewModel
    private lateinit var serieData: MarvelResponse
    private lateinit var layoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_serie)
        viewModel = ViewModelProvider(this).get(DetailSeriesViewModel::class.java)
        binding.lifecycleOwner = this


        layoutManager = GridLayoutManager(this, 1)
        val adapter = DataAdapter(object : DataAdapter.OnClicks{
            override fun onClickCard(id: String) {
                TODO("Not yet implemented")
            }
        });



        val itemUri = intent.getStringExtra("ITEM_ID")

        viewModel.getSerieById(itemUri)

        viewModel.serieData.observe(this, Observer {
            serieData = it
            serieData.toString()
            showData()
        })

    }

    private fun showData() {

        val item = serieData.data.results[0]
        binding.serieNameActivity.text = item.title

        if (item.startYear == null) {
            binding.textStartYear.text = "-"
        } else {
            binding.textStartYear.text = item.startYear.toString()
        }

        if (item.description == null) {
            binding.textviewBioSerieActivity.text = "That serie has no description"
        } else {
            binding.textviewBioSerieActivity.text = item.description as CharSequence?
        }

        if (item.endYear == null) {
            binding.textEndYear.text = "-"
        } else {
            binding.textEndYear.text = item.endYear.toString()
        }

        var imagePath = ""
        try {
            imagePath = item.thumbnail.path + "/portrait_xlarge." + item.thumbnail.extension
        } catch (e: Exception) {
            imagePath = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available" + ".jpg"
        }
        Picasso.get().load(imagePath).fit().centerCrop().into(binding.serieActivityImage)
        
    }
}
