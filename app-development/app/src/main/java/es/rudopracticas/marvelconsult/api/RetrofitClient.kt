package es.rudopracticas.marvelconsult.api

import android.util.Log
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import es.rudopracticas.marvelconsult.BuildConfig
import es.rudopracticas.marvelconsult.helpers.Constants.API_PAGINATION_LIMIT
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.SocketTimeoutException

class RetrofitClient {

    enum class ErrorType {
        HTTPEXCEPTION,  // HTTP
        NETWORK,        // IO
        TIMEOUT,        // Socket
        UNKNOWN         //Anything else
    }

    interface RemoteEmitter {
        fun onResponse(response: Response<Any>)
        fun onError(errorType: ErrorType, msg: String)
    }

    private val client by lazy {
        Retrofit.Builder()
            .baseUrl(Config.API_URL)
            .client(
                OkHttpClient()
                    .newBuilder()
                    .addInterceptor(
                        LoggingInterceptor.Builder()
                            .loggable(BuildConfig.DEBUG)
                            .setLevel(Level.BODY)
                            .request("Request")
                            .response("Response")
                            .addHeader("Accept", "application/json")
                            .build()
                    )
                    .build()
            )
            //.addConverterFactory(MoshiConverterFactory.create()) //Moshi
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(Api::class.java)
    }

    suspend inline fun <T> apiCall(
        crossinline responseFunction: suspend () -> T,
        emitter: RemoteEmitter
    ) {
        try {
            val response = withContext(Dispatchers.IO) { responseFunction.invoke() }
            withContext(Dispatchers.Main) {
                emitter.onResponse(response as Response<Any>)
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                e.printStackTrace()
                Log.e("ApiCalls", "Call error: ${e.localizedMessage}", e.cause)
                when (e) {
                    is HttpException -> {
                        val body = e.response()?.errorBody().toString()
                        emitter.onError(ErrorType.HTTPEXCEPTION, body)
                    }
                    is SocketTimeoutException -> emitter.onError(
                        ErrorType.TIMEOUT,
                        "Timeout Error"
                    )
                    is IOException -> emitter.onError(ErrorType.NETWORK, "Thread Error")
                    else -> emitter.onError(ErrorType.UNKNOWN, "Unknown Error")
                }
            }
        }
    }


    suspend fun getComics(offset: Int, timestamp: Int, apikey: String, hash: String) =
        client.getComics("title", API_PAGINATION_LIMIT, offset, timestamp, apikey, hash, true)

    suspend fun getComicsQueryTitle(
        offset: Int,
        timestamp: Int,
        apikey: String,
        hash: String,
        query: String
    ) =
        client.getComicsQueryTitle(
            "title",
            API_PAGINATION_LIMIT,
            offset,
            timestamp,
            apikey,
            hash,
            true,
            query
        )

    suspend fun getCharacters(offset: Int, timestamp: Int, apikey: String, hash: String) =
        client.getCharecters("name", API_PAGINATION_LIMIT, offset, timestamp, apikey, hash)

    suspend fun getCharactersQueryName(
        offset: Int,
        timestamp: Int,
        apikey: String,
        hash: String,
        name: String
    ) =
        client.getCharectersQueryName(
            "name",
            API_PAGINATION_LIMIT,
            offset,
            timestamp,
            apikey,
            hash,
            name
        )

    suspend fun getCreators(offset: Int, timestamp: Int, apikey: String, hash: String) =
        client.getCreators("firstName", API_PAGINATION_LIMIT, offset, timestamp, apikey, hash)

    suspend fun getCreatorsQueryName(
        offset: Int,
        timestamp: Int,
        apikey: String,
        hash: String,
        name: String
    ) =
        client.getCreatorsQueryName(
            "firstName",
            API_PAGINATION_LIMIT,
            offset,
            timestamp,
            apikey,
            hash,
            name
        )

    suspend fun getSeries(offset: Int, timestamp: Int, apikey: String, hash: String) =
        client.getSeries("title", API_PAGINATION_LIMIT, offset, timestamp, apikey, hash)

    suspend fun getSeriesQueryTitle(
        offset: Int,
        timestamp: Int,
        apikey: String,
        hash: String,
        title: String
    ) =
        client.getSeriesQueryTitle(
            "title",
            API_PAGINATION_LIMIT,
            offset,
            timestamp,
            apikey,
            hash,
            title
        )

    suspend fun getDataWithURI(
        id: String,
        timestamp: Int,
        apikey: String,
        hash: String
    ) = client.getDataWithURI(
        id,
        timestamp,
        apikey,
        hash
    )

}