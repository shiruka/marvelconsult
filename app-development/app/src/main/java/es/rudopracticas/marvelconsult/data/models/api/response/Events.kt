package es.rudopracticas.marvelconsult.data.models.api.response

data class Events(
    val available: Int,
    val collectionURI: String,
    val items: List<Any>,
    val returned: Int
)