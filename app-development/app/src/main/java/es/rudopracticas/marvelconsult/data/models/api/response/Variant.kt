package es.rudopracticas.marvelconsult.data.models.api.response

data class Variant(
    val name: String,
    val resourceURI: String
)