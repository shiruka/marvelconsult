package es.rudopracticas.marvelconsult.data.models.api.response

data class Thumbnail(
    val extension: String,
    val path: String
)