package es.rudopracticas.marvelconsult.modules.marvel_content.profile_fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseAuth
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.databinding.FragmentProfileBinding
import es.rudopracticas.marvelconsult.modules.login.LoginActivity

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var viewModel: ProfileViewModel
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        binding.lifecycleOwner = this

        auth = FirebaseAuth.getInstance()

        binding.usernameText.text = auth.currentUser?.email

        binding.buttonLogoutProfile.setOnClickListener {
            LogOutAlertBuilder()
        }
        return binding.root
    }

    private fun LogOutAlertBuilder() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(getString(R.string.text_alertLogOut_title))
        builder.setMessage(getString(R.string.text_alertLogOut_message))

        builder.setPositiveButton(getString(R.string.text_alertLogOut_confirmation)) { dialog, which ->
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(activity, LoginActivity::class.java))
            activity?.finish()
        }
        builder.setNegativeButton(getString(R.string.text_alertLogOut_cancel)) { dialogInterface: DialogInterface, i: Int ->
            dialogInterface.dismiss()
        }
        builder.create()
        builder.show()
    }

}
