package es.rudopracticas.marvelconsult.data.models.api.response

data class Date(
    val date: String,
    val type: String
)