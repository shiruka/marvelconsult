package es.rudopracticas.marvelconsult.modules.marvel_content.series_detail_fragment

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudopracticas.marvelconsult.api.Config
import es.rudopracticas.marvelconsult.api.RetrofitClient
import es.rudopracticas.marvelconsult.data.models.api.response.MarvelResponse
import es.rudopracticas.marvelconsult.helpers.Constants
import es.rudopracticas.marvelconsult.helpers.extensions.md5
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class DetailSeriesViewModel : ViewModel() {

    val serieData = MutableLiveData<MarvelResponse>()

    private val ts = 1
    private val hash = ts.toString() + Config.PRIVATE_KEY + Config.PUBLIC_KEY

     fun getSerieById(id: String){
        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                RetrofitClient().getDataWithURI(id, ts, Config.PUBLIC_KEY, hash.md5())
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            serieData.postValue(response.body() as MarvelResponse)
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }
}