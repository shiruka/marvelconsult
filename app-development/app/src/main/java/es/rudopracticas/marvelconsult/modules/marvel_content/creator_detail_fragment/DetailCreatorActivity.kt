package es.rudopracticas.marvelconsult.modules.marvel_content.creator_detail_fragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import es.rudopracticas.marvelconsult.DataBinderMapperImpl
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.data.models.api.response.MarvelResponse
import es.rudopracticas.marvelconsult.databinding.ActivityDetailCreatorBinding

class DetailCreatorActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailCreatorBinding
    private lateinit var viewModel: DetailCreatorViewModel
    private lateinit var creatorData: MarvelResponse
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_creator)
        viewModel = ViewModelProvider(this).get(DetailCreatorViewModel::class.java)
        binding.lifecycleOwner = this

        val itemUri = intent.getStringExtra("ITEM_ID")

        viewModel.getCreatorById(itemUri)

        viewModel.creatorData.observe(this, Observer {
            creatorData = it
            creatorData.toString()
            showData()
        })

    }

    private fun showData(){
        val item = creatorData.data.results[0]

        binding.creatorNameActivity.text =item.fullName

        val imagePath = item.thumbnail.path + "/portrait_xlarge." + item.thumbnail.extension
        Picasso.get().load(imagePath).fit().centerCrop().into(binding.creatorImageActivity)

    }
}
