package es.rudopracticas.marvelconsult.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import es.rudopracticas.marvelconsult.data.models.api.response.Results
import es.rudopracticas.marvelconsult.databinding.PrincipalCardItemBinding
import java.lang.Exception


class DataAdapter(private val clickListener: OnClicks) :
    ListAdapter<Any, DataAdapter.ComicViewHolder>(ComicDiffUtilCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComicViewHolder {
        return ComicViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ComicViewHolder, position: Int) {
        val item = getItem(position) as Results
        holder.bind(item, clickListener)
    }

    class ComicViewHolder(private val binding: PrincipalCardItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Results, clicks: OnClicks) {

            binding.executePendingBindings()

            var imagePath = ""

            try {
                imagePath = item.thumbnail.path + "/portrait_xlarge." + item.thumbnail.extension
            } catch (e: Exception) {
                imagePath = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available" + ".jpg"
            }

            Picasso.get().load(imagePath).fit().centerInside().into(binding.cardItemImage)

            // imagePath = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available"

            binding.cardItemImage.setOnClickListener {
                clicks.onClickCard(item.resourceURI)
            }

        }

        companion object {
            fun from(parent: ViewGroup): ComicViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = PrincipalCardItemBinding.inflate(layoutInflater, parent, false)
                return ComicViewHolder(binding)
            }
        }

    }

    class ComicDiffUtilCallback : DiffUtil.ItemCallback<Any>() {
        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            return oldItem == newItem
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            return oldItem == newItem
        }

    }

    interface OnClicks {

        fun onClickCard(id: String)

    }

}