package es.rudopracticas.marvelconsult.data.models.api.response

data class Image(
    val extension: String,
    val path: String
)