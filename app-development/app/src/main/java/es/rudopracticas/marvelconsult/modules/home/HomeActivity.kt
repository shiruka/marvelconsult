package es.rudopracticas.marvelconsult.modules.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.databinding.ActivityHomeBinding
import es.rudopracticas.marvelconsult.modules.marvel_content.character_fragment.CharactersFragment
import es.rudopracticas.marvelconsult.modules.marvel_content.comic_fragment.ComicsFragment
import es.rudopracticas.marvelconsult.modules.marvel_content.creator_fragment.CreatorsFragment
import es.rudopracticas.marvelconsult.modules.marvel_content.profile_fragment.ProfileFragment
import es.rudopracticas.marvelconsult.modules.marvel_content.series_fragment.SeriesFragment

private const val TAG_ONE = "first"
private const val TAG_SECOND = "second"
private const val TAG_THIRD = "third"
private const val TAG_FOURTH = "fourth"
private const val TAG_FIFTH = "fifth"

class HomeActivity : AppCompatActivity() {
    private var currentTag: String = TAG_ONE
    var currentMenuItemId: Int = R.id.button_comics
    private var oldTag: String = TAG_ONE
    private var currentFragment: Fragment = ComicsFragment()


    private lateinit var binding: ActivityHomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        binding.lifecycleOwner = this

        if (savedInstanceState == null) loadFirstFragment()

        chooseFragment()
    }

    private fun chooseFragment() {

        binding.bottomMenu.setOnNavigationItemSelectedListener { item ->

            if (currentMenuItemId != item.itemId) {
                val fragment: Fragment
                oldTag = currentTag

                currentMenuItemId = item.itemId

                when (currentMenuItemId) {
                    R.id.button_comics -> {
                        currentTag = TAG_ONE
                        fragment = ComicsFragment()
                        loadFragment(fragment, currentTag)
                    }
                    R.id.button_characters -> {
                        currentTag = TAG_SECOND
                        fragment =
                            CharactersFragment()
                        loadFragment(fragment, currentTag)
                    }

                    R.id.button_creators -> {
                        currentTag = TAG_THIRD
                        fragment =
                            CreatorsFragment()
                        loadFragment(fragment, currentTag)
                    }
                    R.id.button_series -> {
                        currentTag = TAG_FOURTH
                        fragment =
                            SeriesFragment()
                        loadFragment(fragment, currentTag)
                    }
                    R.id.button_profile -> {
                        currentTag = TAG_FIFTH
                        fragment =
                            ProfileFragment()
                        loadFragment(fragment, currentTag)
                    }

                }
                return@setOnNavigationItemSelectedListener true
            }
            false
        }

    }

    private fun loadFragment(fragment: Fragment, tag: String) {

        if (currentFragment !== fragment) {
            val ft = supportFragmentManager.beginTransaction()

            if (fragment.isAdded) {
                ft.hide(currentFragment).show(fragment)
            } else {
                ft.hide(currentFragment).add(R.id.fragment_container, fragment, tag)
            }
            currentFragment = fragment

            ft.commit()

        }

    }

    private fun loadFirstFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        currentFragment = ComicsFragment()
        transaction.add(R.id.fragment_container, currentFragment, TAG_ONE)
        transaction.commit()
    }
}
