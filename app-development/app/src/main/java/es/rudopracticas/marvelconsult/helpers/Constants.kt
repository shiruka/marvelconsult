package es.rudopracticas.marvelconsult.helpers

object Constants {

    const val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    const val SERVER_SUCCESS_CODE = 200
    const val SERVER_CREATED_CODE = 201
    const val SERVER_NOCONTENT_CODE = 204
    const val SERVER_BADREQUEST_CODE = 400
    const val SERVER_UNAUTHORIZED_CODE = 401
    const val SERVER_FORBIDDEN_CODE = 403
    const val SERVER_NOTFOUND_CODE = 404
    const val SERVER_TIMEOUT_CODE = 408
    const val SERVER_INTERNALSERVER_CODE = 500

    const val API_DATATYPE_COMIC = "COMIC"
    const val API_DATATYPE_CHARACTER = "CHARACTER"
    const val API_DATATYPE_CREATOR = "CREATOR"
    const val API_DATATYPE_SERIES = "SERIES"

    const val API_PAGINATION_LIMIT = 50

}