package es.rudopracticas.marvelconsult.data.models.api.response

data class Stories(
    val available: Int,
    val collectionURI: String,
    val items: List<Item>,
    val returned: Int
)