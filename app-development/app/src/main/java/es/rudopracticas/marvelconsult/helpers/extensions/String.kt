package es.rudopracticas.marvelconsult.helpers.extensions

import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.widget.AppCompatEditText
import es.rudopracticas.marvelconsult.helpers.Constants
import kotlinx.coroutines.*
import java.math.BigInteger
import java.security.MessageDigest

fun String.isValidEmail() = matches(Constants.EMAIL_PATTERN.toRegex())

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}

