package es.rudopracticas.marvelconsult.modules.marvel_content.comic_detail_fragment

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.adapters.DataAdapter
import es.rudopracticas.marvelconsult.data.models.api.response.MarvelResponse
import es.rudopracticas.marvelconsult.databinding.ActivityDetailComicBinding
import es.rudopracticas.marvelconsult.modules.marvel_content.creator_detail_fragment.DetailCreatorActivity
import es.rudopracticas.marvelconsult.modules.marvel_content.series_detail_fragment.DetailSerieActivity

class DetailComicActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailComicBinding
    private lateinit var viewModel: DetailComicViewModel
    private lateinit var comicData: MarvelResponse

    private lateinit var creatorsAdapter: DataAdapter
    private lateinit var seriesAdaper: DataAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_comic)
        viewModel = ViewModelProvider(this).get(DetailComicViewModel::class.java)
        binding.lifecycleOwner = this

        val itemUri = intent.getStringExtra("ITEM_ID")

        viewModel.getDataByURI(itemUri, "COMIC")

        viewModel.comicData.observe(this, Observer {
            comicData = it
            showData()
        })

        creatorsAdapter = DataAdapter(object : DataAdapter.OnClicks {
            override fun onClickCard(id: String) {
                val intent = Intent(applicationContext, DetailCreatorActivity::class.java)
                intent.putExtra("ITEM_ID", id)
                startActivity(intent)
            }
        })

        binding.creatorsRecycler.adapter = creatorsAdapter
        binding.creatorsRecycler.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        seriesAdaper = DataAdapter(object : DataAdapter.OnClicks {
            override fun onClickCard(id: String) {
                val intent = Intent(applicationContext, DetailSerieActivity::class.java)
                intent.putExtra("ITEM_ID", id)
                startActivity(intent)
            }
        })

        binding.seriesRecycler.adapter = seriesAdaper
        binding.seriesRecycler.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

    }

    private fun showData() {
        val item = comicData.data.results[0]
        binding.comicName.text = item.title

        if (item.description == null) {
            binding.descriptionComic.text = "This comic has not description"
        } else {
            binding.descriptionComic.text = item.description as CharSequence?
        }

        if (item.pageCount == null) {
            binding.pageCount.text = "Unknown"
        } else {
            binding.pageCount.text = item.pageCount.toString()
        }

        val imagePath = item.thumbnail.path + "/portrait_xlarge." + item.thumbnail.extension
        Picasso.get().load(imagePath).fit().centerCrop().into(binding.comicImage)

        viewModel.creatorsData.observe(this, Observer {
            creatorsAdapter.submitList(it.data.results as List<Any>?)
            creatorsAdapter.notifyDataSetChanged()
        })

        viewModel.getDataByURI(comicData.data.results[0].creators.collectionURI, "CREATOR")

        viewModel.storyData.observe(this, Observer {
            seriesAdaper.submitList(it.data.results as List<Any>?)
            seriesAdaper.notifyDataSetChanged()
        })

        viewModel.getDataByURI(comicData.data.results[0].series.resourceURI, "SERIE")

    }
}
