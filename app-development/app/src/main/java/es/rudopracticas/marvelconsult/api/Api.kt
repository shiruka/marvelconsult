package es.rudopracticas.marvelconsult.api

import es.rudopracticas.marvelconsult.data.models.api.response.MarvelResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface Api {

    @GET("comics")
    suspend fun getComics(
        @Query("orderBy") orderBy: String, //title
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("ts") timestamp: Int,
        @Query("apikey") apikey: String,
        @Query("hash") md5: String,
        @Query("noVariants") noVariant: Boolean
    ): Response<MarvelResponse>

    @GET("comics")
    suspend fun getComicsQueryTitle(
        @Query("orderBy") orderBy: String, //title
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("ts") timestamp: Int,
        @Query("apikey") apikey: String,
        @Query("hash") md5: String,
        @Query("noVariants") noVariant: Boolean,
        @Query("titleStartsWith") search: String
    ): Response<MarvelResponse>

    @GET("characters")
    suspend fun getCharecters(
        @Query("orderBy") orderBy: String, //name
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("ts") timestamp: Int,
        @Query("apikey") apikey: String,
        @Query("hash") md5: String
    ): Response<MarvelResponse>

    @GET("characters")
    suspend fun getCharectersQueryName(
        @Query("orderBy") orderBy: String, //name
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("ts") timestamp: Int,
        @Query("apikey") apikey: String,
        @Query("hash") md5: String,
        @Query("nameStartsWith") name: String
    ): Response<MarvelResponse>

    @GET("creators")
    suspend fun getCreators(
        @Query("orderBy") orderBy: String, //first name
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("ts") timestamp: Int,
        @Query("apikey") apikey: String,
        @Query("hash") md5: String
    ): Response<MarvelResponse>

    @GET("creators")
    suspend fun getCreatorsQueryName(
        @Query("orderBy") orderBy: String, //name
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("ts") timestamp: Int,
        @Query("apikey") apikey: String,
        @Query("hash") md5: String,
        @Query("nameStartsWith") name: String
    ): Response<MarvelResponse>

    @GET("series")
    suspend fun getSeries(
        @Query("orderBy") orderBy: String, //title
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("ts") timestamp: Int,
        @Query("apikey") apikey: String,
        @Query("hash") md5: String
    ): Response<MarvelResponse>

    @GET("series")
    suspend fun getSeriesQueryTitle(
        @Query("orderBy") orderBy: String, //title
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("ts") timestamp: Int,
        @Query("apikey") apikey: String,
        @Query("hash") md5: String,
        @Query("titleStartsWith") title: String
    ): Response<MarvelResponse>

    @GET
    suspend fun getDataWithURI(
        @Url resourceUri: String,
        @Query("ts") timestamp: Int,
        @Query("apikey") apikey: String,
        @Query("hash") md5: String
    ): Response<MarvelResponse>

}