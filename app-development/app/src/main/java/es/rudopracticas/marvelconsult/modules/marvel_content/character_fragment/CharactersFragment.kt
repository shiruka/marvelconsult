package es.rudopracticas.marvelconsult.modules.marvel_content.character_fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.adapters.DataAdapter
import es.rudopracticas.marvelconsult.databinding.FragmentCharactersBinding
import es.rudopracticas.marvelconsult.helpers.PaginationScrollListener
import es.rudopracticas.marvelconsult.modules.login.LoginActivity
import es.rudopracticas.marvelconsult.modules.marvel_content.character_detail_fragment.DetailCharacterActivity

class CharactersFragment : Fragment() {

    private lateinit var binding: FragmentCharactersBinding
    private lateinit var viewModel: CharacterViewModel
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var lastQuery: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_characters, container, false)
        viewModel = ViewModelProvider(this).get(CharacterViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        binding.progressBarCharacters.visibility = View.VISIBLE

        viewModel.eventRefreshing.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it) {
                binding.progressBarCharacters.visibility = View.VISIBLE
            } else {
                binding.progressBarCharacters.visibility = View.INVISIBLE
            }
        })

        binding.buttonLogOut.setOnClickListener {

            val builder = AlertDialog.Builder(activity)
            builder.setTitle(getString(R.string.text_alertLogOut_title))
            builder.setMessage(getString(R.string.text_alertLogOut_message))

            builder.setPositiveButton(getString(R.string.text_alertLogOut_confirmation)) { dialog, which ->
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(activity, LoginActivity::class.java))
                activity?.finish()
            }
            builder.setNegativeButton(getString(R.string.text_alertLogOut_cancel)) { dialogInterface: DialogInterface, i: Int ->
                dialogInterface.dismiss()
            }
            builder.create()
            builder.show()

        }

        layoutManager = GridLayoutManager(context, 3)

        val adapter = DataAdapter(object : DataAdapter.OnClicks {
            override fun onClickCard(id: String) {
                val intent = Intent(activity, DetailCharacterActivity::class.java)
                intent.putExtra("ITEM_ID",id)
                activity?.startActivity(intent)
            }
        })

        binding.recyclerCharacters.adapter = adapter
        binding.recyclerCharacters.layoutManager = layoutManager

        viewModel.characterData.observe(this.viewLifecycleOwner, androidx.lifecycle.Observer {
            it?.let {
                adapter.submitList(it.data.results as List<Any>?)
                adapter.notifyDataSetChanged()
            }
        })

        viewModel.query.observe(this.viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it.isNotEmpty()) {
                viewModel.isQuery = true
                viewModel.offset = 0
                lastQuery = it
                viewModel.getCharactersQueryName(it)
            } else {
                viewModel.offset = 0
                viewModel.getCharacters()
            }
        })

        binding.recyclerCharacters.addOnScrollListener(object :
            PaginationScrollListener(layoutManager as GridLayoutManager) {

            override fun isLoading(): Boolean {
                return viewModel.eventRefreshing.value!!
            }

            override fun loadMoreItems() {
                if (!isLoading()) {
                    viewModel.offset += 50
                    if (viewModel.isQuery) {
                        viewModel.getCharactersQueryName(lastQuery)
                    } else {
                        viewModel.getCharacters()
                    }
                }
            }

        })

        return binding.root
    }

}
