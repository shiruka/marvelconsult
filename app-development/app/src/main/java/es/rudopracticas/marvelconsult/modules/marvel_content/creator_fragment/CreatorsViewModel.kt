package es.rudopracticas.marvelconsult.modules.marvel_content.creator_fragment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudopracticas.marvelconsult.api.Config
import es.rudopracticas.marvelconsult.api.RetrofitClient
import es.rudopracticas.marvelconsult.data.models.api.response.MarvelResponse
import es.rudopracticas.marvelconsult.helpers.Constants
import es.rudopracticas.marvelconsult.helpers.extensions.md5
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class CreatorsViewModel : ViewModel() {

    val creatorData = MutableLiveData<MarvelResponse>()
    val query = MutableLiveData<String>()

    var isQuery = false
    var offset = 0
    private val ts = 1

    private var _eventIsRefreshing = MutableLiveData<Boolean>()
    val eventRefreshing: LiveData<Boolean>
        get() = _eventIsRefreshing

    private val hash = ts.toString() + Config.PRIVATE_KEY + Config.PUBLIC_KEY

    init {
        getCreators()
    }

    fun getCreators() {
        _eventIsRefreshing.value = true
        CoroutineScope(Dispatchers.IO).launch {

            RetrofitClient().apiCall({
                RetrofitClient().getCreators(offset, ts, Config.PUBLIC_KEY, hash.md5())
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE && offset == 0) {
                            creatorData.value = response.body() as MarvelResponse?
                            creatorData.value?.data?.dataType = Constants.API_DATATYPE_CREATOR
                        } else if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val mr = response.body() as MarvelResponse
                            val aux = creatorData.value
                            aux?.data?.results?.addAll(mr.data.results)
                            aux?.data?.dataType = Constants.API_DATATYPE_CREATOR
                            creatorData.postValue(aux)
                        }

                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }

                }
            )
            withContext(Dispatchers.Main) {
                _eventIsRefreshing.value = false
            }
        }
    }

    fun getCreatorsQueryName(searchQuery: String) {
        _eventIsRefreshing.value = true
        CoroutineScope(Dispatchers.IO).launch {

            RetrofitClient().apiCall({
                RetrofitClient().getCreatorsQueryName(
                    offset, ts, Config.PUBLIC_KEY, hash.md5(), searchQuery
                )
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE && offset == 0) {
                            creatorData.value = response.body() as MarvelResponse?
                            creatorData.value?.data?.dataType = Constants.API_DATATYPE_CREATOR
                        } else if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val mr = response.body() as MarvelResponse
                            val aux = creatorData.value
                            aux?.data?.results?.addAll(mr.data.results)
                            aux?.data?.dataType = Constants.API_DATATYPE_CREATOR
                            creatorData.postValue(aux)
                        }

                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }

                }
            )
            withContext(Dispatchers.Main) {
                _eventIsRefreshing.value = false
            }
        }
    }

}