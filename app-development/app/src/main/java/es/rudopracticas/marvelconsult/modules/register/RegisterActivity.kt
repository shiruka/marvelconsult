package es.rudopracticas.marvelconsult.modules.register

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.firebase.auth.FirebaseAuth
import es.rudopracticas.marvelconsult.R
import es.rudopracticas.marvelconsult.databinding.ActivityRegisterBinding
import es.rudopracticas.marvelconsult.helpers.extensions.isValidEmail
import es.rudopracticas.marvelconsult.modules.login.LoginActivity

class RegisterActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        auth = FirebaseAuth.getInstance()

        initListeners()
    }

    private fun initListeners() {
        binding.buttonSignup.setOnClickListener {
            checkRegister()
        }
        binding.buttonGotoLogin.setOnClickListener {
            intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun checkRegister() {
        if (binding.edittextUsernameRegister.text.toString().isEmpty()) {
            binding.edittextUsernameRegister.error = getString(R.string.text_errorInput_enterEmail)
            binding.edittextUsernameRegister.requestFocus()
            return
        }

        if (!binding.edittextUsernameRegister.text.toString().isValidEmail()
        ) {

            binding.edittextUsernameRegister.error = getString(R.string.text_errorInput_enterEmailValid)
            binding.edittextUsernameRegister.requestFocus()
            return
        }
        if (binding.edittextPasswordRegister.text.toString().isEmpty()) {
            binding.edittextPasswordRegister.error = getString(R.string.text_errorInput_enterPassword)
            binding.edittextPasswordRegister.requestFocus()
            return
        }
        if (binding.edittextPasswordRegister.text.toString() != binding.edittextConfirmpasswordRegister.text.toString()
        ) {
            binding.edittextPasswordRegister.error = getString(R.string.text_errorInput_enterPasswordDiff)
            binding.edittextPasswordRegister.requestFocus()
            return
        }

        auth.createUserWithEmailAndPassword(
            binding.edittextUsernameRegister.text.toString(),
            binding.edittextPasswordRegister.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("SUCCESS", "createUserWithEmail:success")
                    startActivity(Intent(this, LoginActivity::class.java))
                    Toast.makeText(
                        baseContext, getString(R.string.text_registerShow_registrationSuccess),
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                } else {
                    Log.w("FAILED", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, getString(R.string.text_registerShow_registrationFail),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }
}


